import socket
import pickle

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  # создаем сокет
sock.bind(('', 55000))  # связываем сокет с портом, где он будет ожидать сообщения
sock.listen(10)  # указываем сколько может сокет принимать соединений
conn, addr = sock.accept()

while True:
    message = input('me: ')
    conn.send(message.encode())
    message = conn.recv(1024)
    message = message.decode()
    print(addr, ':', message)

conn.close()  # закрываем соединение