import pygame
import math
import time
import random
import sys
from wolfensteinV.player import Player
from wolfensteinV.npc import NPC
from wolfensteinV.bullets import Bullet, Bullet_NPC
from wolfensteinV.walls import Wall, Wall_Company


# ------константы-------
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)
GREY  = (128, 128, 128)

WIDTH = 1920
HEIGHT = 1080
FPS = 60


pygame.init()
pygame.mixer.init()
# ------параметры_экрана------

screen = pygame.display.set_mode((WIDTH, HEIGHT))

pygame.display.set_caption("Castle Wolfenstain 2D")

# ------флаги------

done = False
display_instructions = True
instruction_page = 1
NPC_alarm = False

font = pygame.font.Font(r"C:\Users\Admin\Desktop\program_lessons\wolfenstain\wolfensteinV\font1.ttf", 30)
clock = pygame.time.Clock()
lvl2 = False



# ------инструкции------

while not done and display_instructions:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()
        if event.type == pygame.MOUSEBUTTONDOWN:
            instruction_page += 1
            if instruction_page == 3:
                display_instructions = False

    screen.fill(BLACK)

    if instruction_page == 1:
        text = font.render("Instructions: ", True, WHITE)
        screen.blit(text, [10, 10])

        text = font.render("This game is designed to be similar to Castle Wolfenstain 2D.", True, WHITE)
        screen.blit(text, [10, 40])

        text = font.render("Moving is carried out using the arrows.", True, WHITE)
        screen.blit(text, [10, 70])

        text = font.render("Shooting with the mouse (LMB - shooting at the side of the mouse pointer)", True, WHITE)
        screen.blit(text, [10, 100])

    if instruction_page == 2:
        text = font.render("Good luck!", True, WHITE)
        screen.blit(text, [10, 10])

    pygame.display.flip()





all_NPCs = pygame.sprite.Group()
all_sprites = pygame.sprite.Group()
player = Player()
all_players = pygame.sprite.Group()
npc1 = NPC(1920 - 1700, 1080 - 950)
npc2 = NPC(1920-1700, 950)
all_NPCs.add(npc1, npc2)
bullet_list = pygame.sprite.Group()
bullet_list_NPC = pygame.sprite.Group()

all_players.add(player)
wall_list = pygame.sprite.Group()

wallwall = Wall_Company(wall_list, all_sprites)
wallwall.lvl1()



player.walls = wall_list
npc1.walls = wall_list
npc2.walls = wall_list


def clear_lists(*lists):
    for list_ in lists:
        list_.empty()

def update_lists(*lists):
    for list_ in lists:
        list_.update()
def draw_lists(*elements):
    for element_ in elements:
        element_.draw(screen)


def alarm_in_the_castle_Wolfenstein():

    npc_fire_list = [True, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False]

    for i in all_NPCs:
        npc_fire = random.choice(npc_fire_list)
        if NPC_alarm and i.alive() and player.alive() and npc_fire:
            sprite_pos_x = player.rect.x
            sprite_pos_y = player.rect.y

            bullet_NPC = Bullet_NPC(i.rect.x + 50, i.rect.y + 30, sprite_pos_x, sprite_pos_y)
            bullet_NPC.walls = wall_list

            bullet_list_NPC.add(bullet_NPC)





def game_over_rip():
    player_R_I_P = False

    while not player_R_I_P:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                player_R_I_P = True

        screen.fill(BLACK)

        text = font.render("GyMEOVER", True, GREEN)
        screen.blit(text, [100, 100])

        pygame.display.flip()
        clock.tick(FPS)



    pygame.quit()
    sys.exit()


def game_over_collide():
    player_colide = False
    player = Player()
    player.rect.x = 250
    player.rect.y = 250
    npc1 = NPC(1920 - 1700, 950)
    npc2 = NPC(1920 - 1700, 950)
    npc3 = NPC(1920 - 1700, 950)
    npc4 = NPC(1920 - 1700, 950)
    npc5 = NPC(1920 - 1700, 950)
    npc6 = NPC(1920 - 1700, 950)
    npc7 = NPC(1920 - 1700, 950)
    npc8 = NPC(1920 - 1700, 950)
    npc9 = NPC(1920 - 1700, 950)
    all_players.add(player)
    all_NPCs.add(npc1, npc2, npc3, npc4, npc5, npc6, npc7, npc8, npc9)


    wallwall.game_over_collide_walls()

    player.walls = wall_list
    npc1.walls = wall_list
    npc2.walls = wall_list
    npc3.walls = wall_list
    npc4.walls = wall_list
    npc5.walls = wall_list
    npc6.walls = wall_list
    npc7.walls = wall_list
    npc8.walls = wall_list
    npc9.walls = wall_list

    while not player_colide:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                player_colide = True


        npc1.move()
        npc2.move()
        npc3.move()
        npc4.move()
        npc5.move()
        npc6.move()
        npc7.move()
        npc8.move()
        npc9.move()

        update_lists(all_players, all_NPCs)




        screen.fill(BLACK)

        draw_lists(all_NPCs, all_players, all_sprites)

        text = font.render("GyMEOVER", True, WHITE)
        screen.blit(text, [100, 100])
        pygame.display.flip()
        clock.tick(FPS)
        all_players.update()



    pygame.quit()
    sys.exit()

def up_lvl(x, y):
    if x-20 < player.rect.x < x+20 and y-20 < player.rect.y < y+20:
        return True





def kill():

    for bullet1 in bullet_list:
        block_hit_list = pygame.sprite.spritecollide(bullet1, all_NPCs, True)
        if block_hit_list:
            bullet1.kill()


    for sprite in all_players:
        sprite_hit_list = pygame.sprite.spritecollide(sprite, all_NPCs, True)
        if sprite_hit_list:
            clear_lists(all_players, all_sprites, all_NPCs, bullet_list_NPC, bullet_list, wall_list)
            game_over_collide()

    for NPC_bullet1 in  bullet_list_NPC:
        block_hit_list2 = pygame.sprite.spritecollide(NPC_bullet1, all_players, True)
        if block_hit_list2:
            clear_lists(all_players, all_sprites, all_NPCs, bullet_list_NPC, bullet_list, wall_list)
            game_over_rip()

teleport_to_lvl2 = pygame.image.load(r"C:\Users\Admin\Desktop\program_lessons\wolfenstain\wolfensteinV\pngwing.com.png").convert_alpha()

# ------главный цикл------

while not done:
    for event in pygame.event.get():

        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_LEFT:
                player.x_speed = -3
            elif event.key == pygame.K_RIGHT:
                player.x_speed = 3
            elif event.key == pygame.K_UP:
                player.y_speed = -3
            elif event.key == pygame.K_DOWN:
                player.y_speed = 3

            elif event.key == pygame.K_ESCAPE:
                clear_lists(all_players, all_sprites, all_NPCs, bullet_list_NPC, bullet_list, wall_list)
                done = True




        elif event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()



        elif event.type == pygame.KEYUP:
            if event.key == pygame.K_LEFT or event.key == pygame.K_RIGHT:
                player.x_speed = 0
            elif event.key == pygame.K_UP or event.key == pygame.K_DOWN:
                player.y_speed = 0

        elif event.type == pygame.MOUSEBUTTONDOWN:
            pos = pygame.mouse.get_pos()

            mouse_x = pos[0]
            mouse_y = pos[1]

            NPC_alarm = True

            bullet = Bullet(player.rect.x + 50, player.rect.y + 30, mouse_x, mouse_y)
            bullet.walls = wall_list

            all_sprites.add(bullet)
            bullet_list.add(bullet)

    for i in bullet_list:
        print(i.rect.x, i.rect.y)



    if up_lvl(1000, 310):
        clear_lists(all_players, all_sprites, all_NPCs, bullet_list_NPC, bullet_list, wall_list)

        done = True


    npc1.move()
    npc2.move_rect()
    alarm_in_the_castle_Wolfenstein()
    kill()

    update_lists(bullet_list_NPC, bullet_list, all_NPCs, all_players)



    screen.fill(BLACK)

    draw_lists(all_NPCs, all_players, all_sprites, bullet_list_NPC)
    screen.blit(teleport_to_lvl2, [1000, 310])

    pygame.display.flip()
    clock.tick(FPS)



