"""ШАПКА"""

import pygame
import math
import time
import random
import sys



WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)
GREY  = (128, 128, 128)


WIDTH = 1920
HEIGHT = 1080
FPS = 60

pygame.init()
pygame.mixer.init()
screen = pygame.display.set_mode((WIDTH, HEIGHT))

"""Стрельба"""

class Bullet_NPC(pygame.sprite.Sprite):

    def __init__(self, start_x, start_y, dest_x, dest_y):

        super().__init__()

        self.image = pygame.Surface([4, 4])
        self.image.fill(GREY)

        self.rect = self.image.get_rect()

        self.rect.x = start_x
        self.rect.y = start_y

        self.floating_point_x = start_x
        self.floating_point_y = start_y

        x_diff = dest_x - start_x
        y_diff = dest_y - start_y
        angle = math.atan2(y_diff, x_diff)

        velocity = 10

        self.change_x = math.cos(angle) * velocity
        self.change_y = math.sin(angle) * velocity
        self.walls = None

    def update(self):


        self.floating_point_y += self.change_y
        self.floating_point_x += self.change_x

        self.rect.y = int(self.floating_point_y)
        self.rect.x = int(self.floating_point_x)

        if self.rect.x < 0 or self.rect.x > WIDTH or self.rect.y < 0 or self.rect.y > HEIGHT:
            self.kill()

        block_hit_list = pygame.sprite.spritecollide(self, self.walls, False)
        for block in block_hit_list:
            self.kill()


class Bullet(pygame.sprite.Sprite):

    def __init__(self, start_x, start_y, dest_x, dest_y):

        super().__init__()

        self.image = pygame.Surface([4, 4])
        self.image.fill(GREY)

        self.rect = self.image.get_rect()

        self.rect.x = start_x
        self.rect.y = start_y

        self.floating_point_x = start_x
        self.floating_point_y = start_y

        x_diff = dest_x - start_x
        y_diff = dest_y - start_y
        angle = math.atan2(y_diff, x_diff)

        velocity = 10

        self.change_x = math.cos(angle) * velocity
        self.change_y = math.sin(angle) * velocity
        self.walls = None

    def update(self):


        self.floating_point_y += self.change_y
        self.floating_point_x += self.change_x

        self.rect.y = int(self.floating_point_y)
        self.rect.x = int(self.floating_point_x)

        if self.rect.x < 0 or self.rect.x > WIDTH or self.rect.y < 0 or self.rect.y > HEIGHT:
            self.kill()

        block_hit_list = pygame.sprite.spritecollide(self, self.walls, False)
        for block in block_hit_list:
            self.kill()
