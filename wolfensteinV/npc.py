"""ШАПКА"""

import pygame
import math
import time
import random
import sys



WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)
GREY  = (128, 128, 128)

WIDTH = 1920
HEIGHT = 1080
FPS = 60


pygame.init()
pygame.mixer.init()
screen = pygame.display.set_mode((WIDTH, HEIGHT))

"""Создание NPC"""


NPC_img = pygame.image.load(r"C:\Users\Admin\Desktop\program_lessons\wolfenstain\wolfensteinV\NPC.png").convert()

class NPC(pygame.sprite.Sprite):

    def __init__(self, coordx, coordy):
        pygame.sprite.Sprite.__init__(self)
        self.image = NPC_img
        self.image.set_colorkey(BLACK)
        self.rect = self.image.get_rect()
        self.rect.center = (WIDTH / 2, HEIGHT / 2)
        self.x_speed = 0
        self.y_speed = 0
        self.rect.x = coordx
        self.rect.y = coordy
        self.walls = None


    def changespeedNPC(self):

        self.rect.x += self.x_speed
        self.rect.y += self.y_speed

    def move(self):
        speednpc = random.uniform(0.1, 1)
        moves = [(speednpc, speednpc), (-speednpc, -speednpc), (speednpc, -speednpc), (-speednpc, speednpc)]
        step = random.choice(moves)
        self.x_speed += step[0]
        self.y_speed += step[1]

    def move_rect(self):
        x_cord = self.rect.x
        y_cord = self.rect.y
        move_r = (3, 0)
        move_l = (-3, 0)
        move_u = (0, -3)
        move_d = (0, 3)


        if y_cord > 200 and 210 < x_cord < 230:
            self.x_speed = move_u[0]
            self.y_speed = move_u[1]


        elif x_cord < 1500 and 190 < y_cord < 210:
            self.x_speed = move_r[0]
            self.y_speed = move_r[1]


        elif y_cord < 950 and 1490 < x_cord < 1510:
            self.x_speed = move_d[0]
            self.y_speed = move_d[1]

        elif x_cord > 220 and 940 < y_cord < 960:
            self.x_speed = move_l[0]
            self.y_speed = move_l[1]


        else:
            self.x_speed = 0
            self.y_speed = 0
            print(x_cord, y_cord)




    def update(self):

        self.rect.x += self.x_speed

        block_hit_list = pygame.sprite.spritecollide(self, self.walls, False)
        for block in block_hit_list:

            if self.x_speed > 0:
                self.rect.right = block.rect.left
            else:
                self.rect.left = block.rect.right

        self.rect.y += self.y_speed

        block_hit_list = pygame.sprite.spritecollide(self, self.walls, False)
        for block in block_hit_list:

            if self.y_speed > 0:
                self.rect.bottom = block.rect.top
            else:
                self.rect.top = block.rect.bottom


