import pygame
import math
import time
import random
import sys
from wolfensteinV.player import Player
from wolfensteinV.npc import NPC
from wolfensteinV.bullets import Bullet, Bullet_NPC
from wolfensteinV.walls import Wall, Wall_Company


# ------константы-------
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)
GREY  = (128, 128, 128)

WIDTH = 1920
HEIGHT = 1080
FPS = 60


pygame.init()
pygame.mixer.init()
# ------параметры_экрана------

screen = pygame.display.set_mode((WIDTH, HEIGHT))

pygame.display.set_caption("Castle Wolfenstain 2D")

# ------флаги------

done = False
display_instructions = True
instruction_page = 1
NPC_alarm = False

font = pygame.font.Font(r"C:\Users\Admin\Desktop\program_lessons\wolfenstain\wolfensteinV\font1.ttf", 30)
clock = pygame.time.Clock()
lvl2 = False



# ------инструкции------

while not done and display_instructions:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()
        if event.type == pygame.MOUSEBUTTONDOWN:
            instruction_page += 1
            if instruction_page == 3:
                display_instructions = False

    screen.fill(BLACK)

    if instruction_page == 1:
        text = font.render("Instructions: ", True, WHITE)
        screen.blit(text, [10, 10])

        text = font.render("This game is designed to be similar to Castle Wolfenstain 2D.", True, WHITE)
        screen.blit(text, [10, 40])

        text = font.render("Moving is carried out using the arrows.", True, WHITE)
        screen.blit(text, [10, 70])

        text = font.render("Shooting with the mouse (LMB - shooting at the side of the mouse pointer)", True, WHITE)
        screen.blit(text, [10, 100])

    if instruction_page == 2:
        text = font.render("Good luck!", True, WHITE)
        screen.blit(text, [10, 10])

    pygame.display.flip()





all_NPCs = pygame.sprite.Group()
all_sprites = pygame.sprite.Group()
player = Player()
player2 = Player()
all_players = pygame.sprite.Group()
bullet_list = pygame.sprite.Group()
bullet_list_NPC = pygame.sprite.Group()

all_players.add(player, player2)
wall_list = pygame.sprite.Group()

wallwall = Wall_Company(wall_list, all_sprites)
wallwall.lvl1()



player.walls = wall_list
player2.walls = wall_list




def clear_lists(*lists):
    for list_ in lists:
        list_.empty()

def update_lists(*lists):
    for list_ in lists:
        list_.update()
def draw_lists(*elements):
    for element_ in elements:
        element_.draw(screen)


def up_lvl(x, y):
    if x-20 < player.rect.x < x+20 and y-20 < player.rect.y < y+20:
        return True



teleport_to_lvl2 = pygame.image.load(r"C:\Users\Admin\Desktop\program_lessons\wolfenstain\wolfensteinV\pngwing.com.png").convert_alpha()

# ------главный цикл------

while not done:
    for event in pygame.event.get():

        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_LEFT:
                player.x_speed = -3
            elif event.key == pygame.K_RIGHT:
                player.x_speed = 3
            elif event.key == pygame.K_UP:
                player.y_speed = -3
            elif event.key == pygame.K_DOWN:
                player.y_speed = 3

            elif event.key == pygame.K_a:
                player2.x_speed = -3
            elif event.key == pygame.K_d:
                player2.x_speed = 3
            elif event.key == pygame.K_w:
                player2.y_speed = -3
            elif event.key == pygame.K_s:
                player2.y_speed = 3

            elif event.key == pygame.K_ESCAPE:
                clear_lists(all_players, all_sprites, all_NPCs, bullet_list_NPC, bullet_list, wall_list)
                done = True




        elif event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()



        elif event.type == pygame.KEYUP:
            if event.key == pygame.K_LEFT or event.key == pygame.K_RIGHT:
                player.x_speed = 0
            elif event.key == pygame.K_UP or event.key == pygame.K_DOWN:
                player.y_speed = 0
            elif event.key == pygame.K_a or event.key == pygame.K_d:
                player2.x_speed = 0
            elif event.key == pygame.K_w or event.key == pygame.K_s:
                player2.y_speed = 0




    if up_lvl(1000, 310):
        clear_lists(all_players, all_sprites, all_NPCs, bullet_list_NPC, bullet_list, wall_list)

        done = True


    update_lists(bullet_list_NPC, bullet_list, all_players)



    screen.fill(BLACK)

    draw_lists(all_NPCs, all_players, all_sprites, bullet_list_NPC)
    screen.blit(teleport_to_lvl2, [1000, 310])

    pygame.display.flip()
    clock.tick(FPS)



